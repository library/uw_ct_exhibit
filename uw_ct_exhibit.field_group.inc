<?php
/**
 * @file
 * uw_ct_exhibit.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_exhibit_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_exhibit_image|node|uw_exhibit|form';
  $field_group->group_name = 'group_exhibit_image';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_exhibit';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload an exhibit image',
    'weight' => '8',
    'children' => array(
      0 => 'field_uw_exhibit_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-exhibit-image field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_exhibit_image|node|uw_exhibit|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_upload_file|node|uw_exhibit|form';
  $field_group->group_name = 'group_upload_file';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_exhibit';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload a file',
    'weight' => '7',
    'children' => array(
      0 => 'field_upload_file',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Upload a file',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-upload-file field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_upload_file|node|uw_exhibit|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_upload_image|node|uw_exhibit|form';
  $field_group->group_name = 'group_upload_image';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_exhibit';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload an image to body text',
    'weight' => '6',
    'children' => array(
      0 => 'field_image_to_body',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Upload an image to body text',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-upload-image field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_upload_image|node|uw_exhibit|form'] = $field_group;

  return $export;
}
