INTRODUCTION
------------
The Exhibit module creates an online exhibit that contains some introductory
text (and images), and then some images with their respective metadata:

 * Caption/title (short text field -- will be used in gallery option eventually)
 * Full description (textarea)
 * Date (uses textfield, as sometimes date is "c.19xx" or "19xx-19xx")
 * Unique identifier (e.g. "amalgamation number" for Special Collections)
 * Keywords (uses the Exhibit Tags taxonomy installed by this module)


REQUIREMENTS
------------
This module requires the following modules:
 * CTools
 * Date
 * Features
 * Field_collection
 * Field_group
 * File
 * Image
 * Menu
 * Node
 * Options
 * Override node options
 * Pathauto
 * RDF
 * Schemaorg
 * Search_config
 * Strongarm
 * Taxonomy
 * Text
 * UW vocab term lock
 * Views


INSTALLATION
------------
 * Clone the code from https://git.uwaterloo.ca/graham.faulkner/uw_ct_exhibit.
 * Install the module on your site as per your normal procedures.


CONFIGURATION
-------------
 * No configuration is required.


MAINTAINERS
-----------
Current maintainers:
 * Graham Faulkner - graham.faulkner@uwaterloo.ca


TODO: 
---------------------
 * Add "launch gallery" functionality - would take all images in the
   field_collection and present them in lightbox with titles

