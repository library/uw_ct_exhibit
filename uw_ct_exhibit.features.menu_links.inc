<?php
/**
 * @file
 * uw_ct_exhibit.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_exhibit_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-manager-vocabularies:admin/structure/taxonomy/uw_exhibit_tags
  $menu_links['menu-site-manager-vocabularies:admin/structure/taxonomy/uw_exhibit_tags'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uw_exhibit_tags',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Exhibit tags',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
  );
  // Exported menu link: navigation:node/add/uw-exhibit
  $menu_links['navigation:node/add/uw-exhibit'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/uw-exhibit',
    'router_path' => 'node/add/uw-exhibit',
    'link_title' => 'Exhibit',
    'options' => array(
      'attributes' => array(
        'title' => 'An exhibit contains some introductory text/images, as well as a collection of images, each with their own metadata.',
      ),
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'parent_path' => 'node/add',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Exhibit');
  t('Exhibit tags');


  return $menu_links;
}
