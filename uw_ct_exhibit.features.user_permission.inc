<?php
/**
 * @file
 * uw_ct_exhibit.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_exhibit_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_exhibit content'.
  $permissions['create uw_exhibit content'] = array(
    'name' => 'create uw_exhibit content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_exhibit content'.
  $permissions['delete any uw_exhibit content'] = array(
    'name' => 'delete any uw_exhibit content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_exhibit content'.
  $permissions['delete own uw_exhibit content'] = array(
    'name' => 'delete own uw_exhibit content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in uw_exhibit_tags'.
  $permissions['delete terms in uw_exhibit_tags'] = array(
    'name' => 'delete terms in uw_exhibit_tags',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any uw_exhibit content'.
  $permissions['edit any uw_exhibit content'] = array(
    'name' => 'edit any uw_exhibit content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_exhibit content'.
  $permissions['edit own uw_exhibit content'] = array(
    'name' => 'edit own uw_exhibit content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in uw_exhibit_tags'.
  $permissions['edit terms in uw_exhibit_tags'] = array(
    'name' => 'edit terms in uw_exhibit_tags',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter uw_exhibit revision log entry'.
  $permissions['enter uw_exhibit revision log entry'] = array(
    'name' => 'enter uw_exhibit revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_exhibit authored by option'.
  $permissions['override uw_exhibit authored by option'] = array(
    'name' => 'override uw_exhibit authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_exhibit authored on option'.
  $permissions['override uw_exhibit authored on option'] = array(
    'name' => 'override uw_exhibit authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_exhibit promote to front page option'.
  $permissions['override uw_exhibit promote to front page option'] = array(
    'name' => 'override uw_exhibit promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_exhibit published option'.
  $permissions['override uw_exhibit published option'] = array(
    'name' => 'override uw_exhibit published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_exhibit revision option'.
  $permissions['override uw_exhibit revision option'] = array(
    'name' => 'override uw_exhibit revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_exhibit sticky option'.
  $permissions['override uw_exhibit sticky option'] = array(
    'name' => 'override uw_exhibit sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search uw_exhibit content'.
  $permissions['search uw_exhibit content'] = array(
    'name' => 'search uw_exhibit content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
